<?php

namespace Modules\Cse\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для создания заказа в системе CSE
 *
 * @author Oleg Pyatin
 */
class CseCreateData extends BaseDto
{
    /**
     * @var Клиентский номер сохранённого документа (если есть)
     */
    public $ClientNumber;
    /**
     * @var GUID контактного лица
     */
    public $ContactPerson;
    /**
     * @var Поля составного груза
     */
    public $CargoPackages;
    /**
     * @var Поля отгружаемого товара
     */
    public $Items;
    /**
     * @var Дата забора груза
     */
    public $TakeDate;
    /**
     * @var Время забора груза в формате 00:00 00:00
     */
    public $TakeTime;
    /**
     * @var Желательная дата доставки груза, передавать в формате дата время
     */
    public $DeliveryDate;
    /**
     * @var Желательное время доставки груза в свободном формате
     */
    public $DeliveryTime;
    /**
     * @var Отправитель груза
     */
    public $Sender;
    /**
     * @var GUID географии отправления
     */
    public $SenderGeography;
    /**
     * @var Адрес забора груза
     */
    public $SenderAddress;
    /**
     * @var Контактный телефон отправителя
     */
    public $SenderPhone;
    /**
     * @var Получатель груза
     */
    public $Recipient;
    /**
     * @var GUID географии доставки
     */
    public $RecipientGeography;
    /**
     * @var Контактный телефон получателя
     */
    public $RecipientPhone;
    /**
     * @var Адрес доставки
     */
    public $RecipientAddress;
    /**
     * @var GUID срочности доставки
     */
    public $Urgency;
    /**
     * @var Код плательщика
     */
    public $Payer;
    /**
     * @var Заявленная стоимость груза
     */
    public $DeclaredValueRate;
    /**
     * @var Код способа оплаты
     */
    public $PaymentMethod;
    /**
     * @var GUID способа доставки
     */
    public $ShippingMethod;
    /**
     * @var GUID вида груза
     */
    public $TypeOfCargo;
    /**
     * @var Вес груза в кг
     */
    public $Weight;
    /**
     * @var Высота груза в см
     */
//    public $Height;
    /**
     * @var Длина груза в см
     */
//    public $Length;
    /**
     * @var Ширина груза в см
     */
//    public $Width;
    /**
     * @var Количество мест
     */
    public $CargoPackageQty;
    /**
     * @var Телефон для отправки уведомлений SMS о состоянии доставки
     */
    public $ReplySMSPhone;
    /**
     * @var Адрес электронной почты для отправки уведомлений
     */
    public $ReplyEMail;
    /**
     * @var Адрес электронной почты для отправки уведомлений
     */
    public $ItemsProcessingAction;
    /**
     * @var Описание груза
     */
    public $CargoDescription;
    /**
     * @var Сумма за наложенный платеж
     */
    public $COD;


    // ! Посмотреть насколько актуальны поля

    // Вопрос по ms_id (ID в ИС клиента), order_name (Имя в ИС Клиента)

    /**
     * Карта типов данных для формирования запроса создания заказа
     */
    public const FIELDS_TYPE_MAP = [
        'ContactPerson'=>'string',
        'TakeDate'=>'dateTime',
        'TakeTime'=>'dateTime',
        'DeliveryDate'=>'dateTime',
        'DeliveryTime'=>'string',
        'Sender'=>'string',
        'SenderGeography'=>'string',
        'SenderAddress'=>'string',
        'SenderPhone'=>'string',
        'Recipient'=>'string',
        'RecipientGeography'=>'string',
        'RecipientPhone'=>'string',
        'Urgency'=>'string',
        'Payer'=>'string',
        'PaymentMethod'=>'string',
        'ShippingMethod'=>'string',
        'TypeOfCargo'=>'string',
        'Weight'=>'float',
//        'Height'=>'float',
//        'Length'=>'float',
//        'Width'=>'float',
        'CargoPackageQty'=>'int',
        'ReplySMSPhone'=>'string',
        'ReplyEMail'=>'string',
        'ItemsProcessingAction'=>'string',
        'CargoDescription'=>'string',
        'COD'=>'int', // ! Уточниться по типу COD
        'DeclaredValueRate'=>'float'
    ];

    /**
     * Функция получения подготовленного массива для формирования заказа
     *
     * @return array  Массив с данными для формирования нового заказа
     */
    public function getPreparedData()
    {
        $fields = [];

        foreach (static::FIELDS_TYPE_MAP as $var_name=>$type) {
            $fields[] = [
                'Key'=>$var_name,
                'Value'=>$this->{$var_name},
                'ValueType'=>$type,
            ];
        }

        // Возможно отдельное отслеживание параметра наличия наложенного платежа (если не было то исключаем из полей)

        $order_items = [];
        $order_cargo_packages = [];

        foreach ($this->Items as $item) {
            $order_items[] = $item->getPreparedData();
        }

        foreach ($this->CargoPackages as $cargo_package) {
            $order_cargo_packages[] = $cargo_package->getPreparedData();
        }

        // !!! Здесь сделать заполнение и итоговый просмотр

        //$fields =

        return [
            'SaveDocuments'=>[
                'login'=>getenv("CSE_LOGIN"),
                'password'=>getenv("CSE_PASSWORD"),

                'data'=>[
                    'Key'=>'Orders',
                    'List'=>[
                        'Key'=>'Order',
                        'Properties'=>[
                            'Key'=>'ClientNumber',
                            'Value'=>$this->ClientNumber,
                            'ValueType'=>'string'
                        ],
                        'Fields'=>$fields,
                        'Tables'=>[
                            [
                                'Key'=>'Items',
                                'List'=>$order_items,
                            ],
                            [
                                'Key'=>'CargoPackages',
                                'List'=>$order_cargo_packages,
                            ],
                        ]
                    ],
                ],

                'parameters'=>[
                    'Key'=>'Parameters',
                    'List'=>[
                        'Key'=>'DocumentType',
                        'Value'=>'order',
                        'ValueType'=>'string'
                    ]
                ]
            ]
        ];
    }
}
