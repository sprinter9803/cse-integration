<?php

namespace Modules\Cse\Entities;

/**
 * Класс хранения данных для взаимодействия с SOAP
 *
 * @author Oleg Pyatin
 */
class GetReferenceData
{
    /**
     * @var Переменная указания типа справочных данных
     */
    protected $Reference;

//    protected $login;

//    protected $password;


    public function __construct($Reference, $login, $password) {
        $this->Reference = $Reference;
//        $this->login = $login;
//        $this->password = $password;
//    }

    public function getReference()
    {
        return $this->Reference;
    }

//    public function getLogin()
//    {
//        return $this->login;
//    }
//
//    public function getPassword()
//    {
//        return $this->password;
//    }
}


