<?php

namespace Modules\Cse\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для составных грузов в системе CSE
 *
 * @author Oleg Pyatin
 */
class CsePackageData extends BaseDto
{
    /**
     * @var ID упаковки
     */
    public $PackageID;
    /**
     * @var Количество мест
     */
    public $PackageQty;
    /**
     * @var Вес груза (в кг)
     */
    public $Weight;
    /**
     * @var Высота груза (в см)
     */
    public $Height;
    /**
     * @var Длина груза (в см)
     */
    public $Length;
    /**
     * @var Ширина груза (в см)
     */
    public $Width;

    /**
     * Функция получения подготовленного массива для заполнения данных в заказ
     *
     * @return array  Массив с данными о товаре
     */
    public function getPreparedData()
    {
        return [
            'Key'=>'ItemData',
            'Fields'=>[
                [
                    'Key'=>'PackageID',
                    'Value'=>$this->PackageID,
                    'ValueType'=>'string',
                ],
                [
                    'Key'=>'PackageQty',
                    'Value'=>$this->PackageQty,
                    'ValueType'=>'int',
                ],
                [
                    'Key'=>'Length',
                    'Value'=>$this->Length,
                    'ValueType'=>'float',
                ],
                [
                    'Key'=>'Width',
                    'Value'=>$this->Width,
                    'ValueType'=>'float',
                ],
                [
                    'Key'=>'Height',
                    'Value'=>$this->Height,
                    'ValueType'=>'float',
                ],
                [
                    'Key'=>'Weight',
                    'Value'=>$this->Weight,
                    'ValueType'=>'float',
                ],
            ]
        ];
    }
}
