<?php

namespace Modules\Cse\Entities;

/**
 * DTO-класс хранения данных для взаимодействия с SOAP
 *
 * @author Oleg Pyatin
 */
class SaveDocuments
{
    /**
     * @var Клиентский номер сохранённого документа (если есть)
     */
    public $ClientNumber;
    /**
     * @var GUID контактного лица
     */
    public $ContactPerson;
    /**
     * @var Поля составного груза
     */
    public $CargoPackages;
    /**
     * @var Поля отгружаемого товара
     */
    public $Items;
    /**
     * @var Дата забора груза
     */
    public $TakeDate;
    /**
     * @var Время забора груза в формате 00:00 00:00
     */
    public $TakeTime;
    /**
     * @var Желательная дата доставки груза, передавать в формате дата время
     */
    public $DeliveryDate;
    /**
     * @var Желательное время доставки груза в свободном формате
     */
    public $DeliveryTime;
    /**
     * @var Отправитель груза
     */
    public $Sender;
    /**
     * @var GUID географии отправления
     */
    public $SenderGeography;
    /**
     * @var Адрес забора груза
     */
    public $SenderAddress;
    /**
     * @var Контактный телефон отправителя
     */
    public $SenderPhone;
    /**
     * @var Получатель груза
     */
    public $Recipient;
    /**
     * @var GUID географии доставки
     */
    public $RecipientGeography;
    /**
     * @var Контактный телефон получателя
     */
    public $RecipientPhone;
    /**
     * @var GUID срочности доставки
     */
    public $Urgency;
    /**
     * @var Код плательщика
     */
    public $Payer;
    /**
     * @var Заявленная стоимость груза
     */
    public $DeclaredValueRate;
    /**
     * @var Код способа оплаты
     */
    public $PaymentMethod;
    /**
     * @var GUID способа доставки
     */
    public $ShippingMethod;
    /**
     * @var GUID вида груза
     */
    public $TypeOfCargo;
    /**
     * @var Вес груза в кг
     */
    public $Weight;
    /**
     * @var Высота груза в см
     */
    public $Height;
    /**
     * @var Длина груза в см
     */
    public $Length;
    /**
     * @var Ширина груза в см
     */
    public $Width;
    /**
     * @var Количество мест
     */
    public $CargoPackageQty;
    /**
     * @var Телефон для отправки уведомлений SMS о состоянии доставки
     */
    public $ReplySMSPhone;
    /**
     * @var Адрес электронной почты для отправки уведомлений
     */
    public $ReplyEMail;
}
