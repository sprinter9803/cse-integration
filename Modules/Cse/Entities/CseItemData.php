<?php

namespace Modules\Cse\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для отгружаемых товаров в системе CSE
 *
 * @author Oleg Pyatin
 */
class CseItemData extends BaseDto
{
    /**
     * @var GUID товара
     */
    public $Item;
    /**
     * @var Цена
     */
    public $Price;
    /**
     * @var Количество
     */
    public $Qty;
    /**
     * @var Оценочная стоимость товара
     */
    public $AssessedValue;
    /**
     * @var Код ставки НДС
     */
    public $VATRate;

    /**
     * Функция получения подготовленного массива для заполнения данных в заказ
     *
     * @return array  Массив с данными о товаре
     */
    public function getPreparedData()
    {
        return [
            'Key'=>'ItemData',
            'Fields'=>[
                [
                    'Key'=>'Item',
                    'Value'=>$this->Item,
                    'ValueType'=>'string',
                ],
                [
                    'Key'=>'Qty',
                    'Value'=>$this->Qty,
                    'ValueType'=>'int',
                ],
                [
                    'Key'=>'Price',
                    'Value'=>$this->Price,
                    'ValueType'=>'float',
                ],
                [
                    'Key'=>'VATRate',
                    'Value'=>$this->VATRate,
                    'ValueType'=>'string',
                ],
                [
                    'Key'=>'AssessedValue',
                    'Value'=>$this->AssessedValue,
                    'ValueType'=>'float',
                ],
            ]
        ];
    }
}
