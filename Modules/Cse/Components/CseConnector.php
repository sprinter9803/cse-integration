<?php

namespace Modules\Cse\Components;

use SoapClient;

/**
 * Компонент для выполнения запросов к API КСЕ
 *
 * @author Oleg Pyatin
 */
class CseConnector
{
    protected $client;

    public function __construct()
    {
        $this->client = new SoapClient(getenv("CSE_SERVER_URL")); 
    }

    public function processQuery(string $function_name, array $query_params)
    {
        return $this->client->__soapCall($function_name, $query_params, NULL);
    }
}