<?php

namespace Modules\Order\Entities;

/**
 * Класс для хранения вспомогательной информации к работе с заказами
 *
 * @author Oleg Pyatin
 */
class OrderProcessValues
{
    /**
     * Вес посылки по-умолчанию (полкилограмма) если не указан
     */
    public const PACKAGE_DEFAULT_WEIGHT = 0.5;

    /**
     * URL для изменения заказов в МойСклад
     */
    public const MW_ORDER_CHANGE_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/';
    /**
     * URL для изменения атрибутов заказа в МойСклад (например указания ошибок, накладной ТК и пр)
     */
    public const MW_ORDER_CHANGE_ATTR_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/attributes/';
    /**
     * URL для изменения статусов заказов в МойСклад
     */
    public const MW_ORDER_CHANGE_STATE_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/states/';

    /**
     * UUID атрибута длины в МС
     */
    public const ATTR_ASSORT_LENGTH = '52e3ea13-ecbd-11e8-9107-50480007f4fa';
    /**
     * UUID атрибута ширины в МС
     */
    public const ATTR_ASSORT_WIDTH = '52e3ed5a-ecbd-11e8-9107-50480007f4fb';
    /**
     * UUID атрибута высоты в МС
     */
    public const ATTR_ASSORT_HEIGHT = '52e3ef2b-ecbd-11e8-9107-50480007f4fc';

    /**
     * Атрибут для получения значения ПВЗ
     */
    public const ATTR_PVZ_CODE = '750d08d4-4340-11ea-0a80-02f700046652';
    /**
     * UUID атрибута адреса покупателя
     */
    public const ATTR_RECIPIENT_MW_ORDER_ADDR = '2a5b2325-49cf-11e6-7a69-97110010a6c2';
    /**
     * Атрибут обозначения что заказ уже оплачен
     */
    public const ATTR_ORDER_IS_PAYED = 'b3417c57-89dd-11e8-9109-f8fc0039ea47';

    /**
     * Атрибут email-а пользователя
     */
    public const MW_ATTR_RECIPIENT_EMAIL = '9dc79c29-cb2f-11e3-fb29-002590a28eca';

    /**
     * Атрибут для получения значения города получателя
     */
    public const MW_ATTR_RECIPIENT_CITY = '132fa987-545d-11e9-9ff4-34e80017a9fa';
    /**
     * Атрибут для хранения телефона
     */
    public const MW_ATTR_RECIPIENT_PHONE = '9dc79b5e-cb2f-11e3-8493-002590a28eca';

    /**
     * Атрибут для адреса доставки
     */
    public const MW_ATTR_RECIPIENT_ADDRESS = '2a5b2325-49cf-11e6-7a69-97110010a6c2';

    /**
     * Атрибут количества мест
     */
    public const MW_ATTR_QUANTITY = 'ffe9b4ad-88ba-11e7-7a6c-d2a90007ca85';

    /**
     * Атрибут даты доставки
     */
    public const MW_ATTR_DELIVERY_DATE = '2598de5e-8528-11e8-9107-5048000c286e';
    /**
     * Атрибут интервала доставки
     */
    public const MW_ATTR_DELIVERY_INTERVAL = '59723787-f3b5-11e8-9109-f8fc00033484';
    /**
     * Аттрибут обозначения что заказ уже оплачен
     */
    public const MW_ATTR_ORDER_IS_PAYED = 'b3417c57-89dd-11e8-9109-f8fc0039ea47';

    /**
     * Атрибут для вывода ошибок
     */
    public const MW_ATTR_ERROR_MESSAGE = 'a31f1635-4fa3-11e9-912f-f3d400039420';
    /**
     * Атрибут в котором мы указываем номер заказа в системе КСЕ (Накладная ТК)
     */
    public const MW_ATTR_TC_ORDER_NUMBER = 'e927642b-88b7-11e7-6b01-4b1d00073fdc';
    /**
     * Ссылка на печать документа накладной
     */
    public const MW_ATTR_CSE_WAYBILL = 'e9a95f3d-4fa2-11e9-9109-f8fc0003db1e';
    /**
     * Атрибут в Мой Склад для дополнительного статуса ТК
     */
    public const MW_ATTR_ADDITIONAL_STATUS = '33309169-b15c-11ea-0a80-073900091d1d';

    /**
     * Обозначение статуса созданного заказа (Используется статус "Согласован")
     */
    public const MW_STATE_CSE_ORDER_CREATED = '7ace2f7d-852d-11e8-9109-f8fc000c6adb';




    /**
     * Контактный телефон отправителя (Колорс Груп или Китчентрэйд)
     */
    public const MAIN_SENDER_PHONE = '89035787484';
    /**
     * Адрес отправителя
     */
    public const MAIN_SENDER_ADDRESS = 'ул. Малая Калужская д 15 стр 17';
    /**
     * Город отправителя (Москва)
     */
    public const MAIN_SENDER_CITY_GUID = 'cf862f56-442d-11dc-9497-0015170f8c09';
    /**
     * Название основного юридического лица отправителя (Колорс)
     */
    public const MAIN_SENDER_COMPANY_NAME = 'ООО "КОЛОРС ГРУП"';
    /**
     * GUID основного юридического лица в системе МС
     */
    public const MAIN_SENDER_MW_GUID = '1479baf9-a1fc-11e8-9ff4-34e800037bd9';
    /**
     * Название дополнительного юридического лица отправителя (Китчентрэйд)
     */
    public const ADDITIVE_SENDER_COMPANY_NAME = 'КИТЧЕНТРЭЙД-ИМ';
    /**
     * GUID дополнительного юридического лица в системе МС
     */
    public const ADDITIVE_SENDER_MW_GUID = '3741a53f-ac72-11e4-90a2-8ecb00088341';

    /**
     * Действие обработки товара по-умолчанию
     */
    public const DEFAULT_ITEMS_PROCESSING_ACTION = 'incoming';

    /**
     * GUID дополнительного юридического лица в системе МС
     */
    public const DELIVERY_TYPE = '662299a9-852c-11e8-9107-5048000c6347';
    /**
     * GUID метода доставки по-умолчанию в системе КСЕ
     */
    public const DEFAULT_SHIPPING_METHOD = 'e45b6d73-fd62-44da-82a6-44eb4d1d9490';
    /**
     * GUID вида груза по-умолчанию в системе КСЕ
     */
    public const DEFAULT_TYPE_OF_CARGO = '4aab1fc6-fc2b-473a-8728-58bcd4ff79ba';
    /**
     * GUID срочности доставки по-умолчанию в системе КСЕ
     */
    public const DEFAULT_URGENCY_GUID = '18c4f207-458b-11dc-9497-0015170f8c09';
    /**
     * Тип плательщика по-умолчанию в системе КСЕ (соответствует значению "Заказчик")
     */
    public const DEFAULT_PAYER_TYPE = '0';
    /**
     * Тип оплаты по-умолчанию (безналичный расчет)
     */
    public const DEFAULT_PAYMENT_METHOD = '1';

    /**
     * Контактное лицо для тестовой среды
     */
    public const TEST_CONTACT_PERSON = '8f417777-9d7d-11e5-b703-005056b1a128';

    /**
     * GUID контактного лица у основного юр.агента (Колорс)
     */
    public const MAIN_COMPANY_CONTACT_PERSON = '65989767-a82b-11eb-80de-0090faaaf5c4';
    /**
     * GUID контактного лица у основного юр.агента (Китчэнтрейд)
     */
    public const ADDITIVE_COMPANY_CONTACT_PERSON = '9f9dcbc5-a82b-11eb-80de-0090faaaf5c4';
    /**
     * Описание посылки по-умолчанию
     */
    public const DEFAULT_DELIVERY_DESCRIPTION = 'Товары';
}
