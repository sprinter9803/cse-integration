<?php

namespace Modules\Order\Services;

use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\Cse\Entities\CseCreateData;
use Modules\Cse\Entities\CseItemData;
use Modules\Cse\Entities\CsePackageData;
use Modules\Cse\Entities\CseProcessValues;
use Modules\Cse\Components\CseConnector;
use Modules\MyWarehouse\Services\MwService;
use Modules\Order\Entities\OrderProcessValues;
use Modules\Order\Exceptions\CseNonExistOrganizationException;

/**
 * Сервис для выполнения распарсивания данных из заказа МС в заказ КСЕ
 *
 * @author Oleg Pyatin
 */
class OrderTransferService
{
    protected $mw_service;

    protected $cse_connector;

    public function __construct(MwService $mw_service, CseConnector $cse_connector)
    {
        $this->mw_service = $mw_service;

        $this->cse_connector = $cse_connector;
    }

    /**
     * Функция получения подготовленных данных для создания заказа
     *
     * @param string $mw_order_data  Данные заказа МойСклад
     * @return CseCreateData  DTO с данными заказа
     */
    public function getOrderData(MwOrderData $mw_order_data)
    {
        $cse_create_data = CseCreateData::loadFromArray([

            'ClientNumber'=>$this->getTestingDevelopUniqueClientNumber(), // ! Здесь нужно будет подставить реальный
            //'ClientNumber'=>$mw_order_data->name,

            'ContactPerson'=>OrderProcessValues::TEST_CONTACT_PERSON,  // ! При переводе на боевой эту строчку убираем (будет заполнение в функции getSenderInfo)

            'Urgency'=>OrderProcessValues::DEFAULT_URGENCY_GUID,

            'Payer'=>OrderProcessValues::DEFAULT_PAYER_TYPE,

            'PaymentMethod'=>OrderProcessValues::DEFAULT_PAYMENT_METHOD,

            'ShippingMethod'=>OrderProcessValues::DEFAULT_SHIPPING_METHOD,

            'TypeOfCargo'=>OrderProcessValues::DEFAULT_TYPE_OF_CARGO,

            'CargoPackageQty'=>$mw_order_data->attributes[OrderProcessValues::MW_ATTR_QUANTITY] ?? '1',

            'ReplySMSPhone'=>$mw_order_data->attributes[OrderProcessValues::MW_ATTR_RECIPIENT_PHONE] ?? $mw_order_data->agent["phone"] ?? '',

            'ReplyEMail'=>$mw_order_data->attributes[OrderProcessValues::MW_ATTR_RECIPIENT_EMAIL] ?? '',

            'ItemsProcessingAction'=>OrderProcessValues::DEFAULT_ITEMS_PROCESSING_ACTION,

            'CargoDescription'=>OrderProcessValues::DEFAULT_DELIVERY_DESCRIPTION,

            'COD'=>0
        ]);

        $this->getDestinationInfo($mw_order_data, $cse_create_data);

        $this->getRecipientInfo($mw_order_data, $cse_create_data);

        $this->getSenderInfo($mw_order_data, $cse_create_data);

        $this->processItemsAndPackageData($mw_order_data, $cse_create_data);

        $this->getDeliveryTimeParamsInfo($mw_order_data, $cse_create_data);

        return $cse_create_data;
    }

    public function processItemsAndPackageData(MwOrderData $mw_order_data, CseCreateData $cse_create_data)
    {
        $items = [];
        $cargo_packages = [];

        $COD = 0; // Сумма (потенциальная) за наложенный платеж
        $declared_value_rate = 0;

        $total_weight = 0;

        foreach ($mw_order_data->positions["rows"] as $index=>$package) {

            $item_type = $position['assortment']['meta']['type'] ?? '';

            $assortment_attributes = $this->mw_service->prepareAssortmentAttrs($package["assortment"]["attributes"] ?? []);

            $price = (((float)$package["price"]) / 100.0);
            $price_discount = $price;
            if (($discount = (int)$package["discount"]) > 0) {
                $price_discount = $price * ( ((float)(100 - $discount)) /100.0);
            }

            if ($item_type !== "service") {
                if (isset($package["assortment"]["weight"])) {
                    $total_weight += $package["assortment"]["weight"] * $package['quantity'];
                }

                $cargo_packages[] = CsePackageData::loadFromArray([
                    'PackageID'=>$this->generateNewPackageId(),
                    'PackageQty'=>'1',

                    'Length'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_LENGTH] ?? '0',
                    'Width'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_WIDTH] ?? '0',
                    'Height'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_HEIGHT] ?? '0',
                    'Weight'=>$package["assortment"]["weight"] ?? OrderProcessValues::PACKAGE_DEFAULT_WEIGHT
                ]);

            } else {

                $package['assortment']['article'] = CseProcessValues::DELIVERY_ITEM_TYPE_ARTICLE;
            }

            $items[] = CseItemData::loadFromArray([
                'Item'=>'article-03-01-037', // ! Временно используем для тестовых целей
//                'Item'=>$package['assortment']['article'] ?: 'tovar'.$index,  // Нужно переставить на этот при переводе на боевую среду
                'Qty'=>$package['quantity'],
                'Price'=>$price_discount,
                'VATRate'=>CseProcessValues::VAT_RATE_TABLE[ ($package['vat'].'%') ],
                'AssessedValue'=>$price_discount * $package["quantity"],
            ]);

            $COD += $price_discount * $package['quantity'];
            $declared_value_rate += $price_discount * $package['quantity'];
        }

        $cse_create_data->Items = $items;
        $cse_create_data->CargoPackages = $cargo_packages;

        $is_order_payed = (bool)($mw_order_data->attributes[OrderProcessValues::ATTR_ORDER_IS_PAYED] ?? false);
        if ($is_order_payed) {
            $cse_create_data->COD = $COD;
        }

        $cse_create_data->DeclaredValueRate = $declared_value_rate;

        $cse_create_data->Weight = $total_weight;
    }


    public function getDestinationInfo(MwOrderData $mw_order_data, CseCreateData $cse_create_data)
    {
        $mw_recipient_city = $mw_order_data->attributes[OrderProcessValues::MW_ATTR_RECIPIENT_CITY] ?? '';

        $prepared_city = trim(str_replace(['г ', 'Г ','г.','Г.'], '', $mw_recipient_city));
        $prepared_city = explode(',', $prepared_city)[0];

        $cse_create_data->RecipientGeography = $this->getCseCityInfo($prepared_city);
    }


    public function getSenderInfo(MwOrderData $mw_order_data, CseCreateData $cse_create_data)
    {
        if ($mw_order_data->organization["id"] === OrderProcessValues::MAIN_SENDER_MW_GUID) {
            $cse_create_data->Sender = OrderProcessValues::MAIN_SENDER_COMPANY_NAME;
            // $cse_create_data->ContactPerson = OrderProcessValues::MAIN_COMPANY_CONTACT_PERSON;  ! Подключаем при переводе в боевую среду
        } elseif ($mw_order_data->organization["id"] === OrderProcessValues::ADDITIVE_SENDER_MW_GUID) {
            $cse_create_data->Sender = OrderProcessValues::ADDITIVE_SENDER_COMPANY_NAME;
            // $cse_create_data->ContactPerson = OrderProcessValues::ADDITIVE_COMPANY_CONTACT_PERSON;  ! Подключаем при переводе в боевую среду
        } else {
            throw new CseNonExistOrganizationException("Order from non recorded organization", 0, null, $mw_order_data->id);
        }

        $cse_create_data->SenderGeography = OrderProcessValues::MAIN_SENDER_CITY_GUID;
        $cse_create_data->SenderAddress = OrderProcessValues::MAIN_SENDER_ADDRESS;
        $cse_create_data->SenderPhone = OrderProcessValues::MAIN_SENDER_PHONE;
    }


    public function getRecipientInfo(MwOrderData $mw_order_data, CseCreateData $cse_create_data)
    {
        $cse_create_data->Recipient = $mw_order_data->agent["name"];

        $cse_create_data->RecipientPhone = $mw_order_data->attributes[OrderProcessValues::MW_ATTR_RECIPIENT_PHONE] ?? $mw_order_data->agent["phone"] ?? '';

        $cse_create_data->RecipientAddress = $mw_order_data->attributes[OrderProcessValues::MW_ATTR_RECIPIENT_ADDRESS] ?? '';
    }


    public function getDeliveryTimeParamsInfo(MwOrderData $mw_order_data, CseCreateData $cse_create_data)
    {
        $delivery_interval = explode('-', $mw_order_data->attributes[OrderProcessValues::MW_ATTR_DELIVERY_INTERVAL]["name"]);

        $take_date = date('Y-m-d H:i:s', strtotime($mw_order_data->deliveryPlannedMoment));

        $cse_create_data->TakeDate = $take_date;
        $cse_create_data->TakeTime = $delivery_interval[0].':00 ' . $delivery_interval[1].':00';
        $cse_create_data->DeliveryTime = intval($delivery_interval[0]) >= 18 ? '18:00 22:00' : $take_date;
        $cse_create_data->DeliveryDate = date('Y-m-d', strtotime($mw_order_data->attributes[OrderProcessValues::MW_ATTR_DELIVERY_DATE])) . 'T' . $delivery_interval[0].':00:00';
    }

    /**
     * Функция получения информации о городе в системе КСЕ
     *
     * @param string $city_name  Предполагаемое название города
     * @return mixed  Данные о городе если есть (null если в базе не нашлось)
     */
    public function getCseCityInfo(string $city_name)
    {
        $params = [
            "GetReferenceData"=>[
                'login'=>'test',
                'password'=>'2016',
                'parameters'=>[
                    'Key'=>'parameters',
                    'List'=>[
                        [
                            'Key'=>'Reference',
                            'Value'=>'Geography',
                            'ValueType'=>'string'
                        ],
                        [
                            'Key'=>'Search',
                            'Value'=>$city_name . ' г',
                            'ValueType'=>'string'
                        ],
                    ]
                ]
            ]
        ];

        $city_info_response = $this->cse_connector->processQuery("GetReferenceData", $params);

        // Случай когда несколько городов одинакового названия (возвращаем первый)
        if (is_array($city_info_response->return->List)) {
            return $city_info_response->return->List[0]->Key;
        }

        return $city_info_response->return->List->Key ?? null;
    }


    /**
     * Функция для получения нового клиентского ID заказа
     *
     * @return string  Новый клиентский номер (ClientNumber)
     */
    public function getTestingDevelopUniqueClientNumber()
    {
        return mt_rand(1,10000000).mt_rand(1,10000000);
    }

    /**
     * Функция для получения нового ID упаковки
     *
     * @return string  Новый ID упаковки (Для массива CargoPackages)
     */
    public function generateNewPackageId()
    {
        return mt_rand(1,100000000).mt_rand(1,100000000);
    }
}
