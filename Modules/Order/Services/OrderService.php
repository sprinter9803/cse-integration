<?php

namespace Modules\Order\Services;

use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\Cse\Entities\CseCreateData;
use Modules\Cse\Components\CseConnector;
use Modules\Order\Services\OrderTransferService;
use Modules\Order\Services\OrderCheckService;
use Modules\Order\Services\OrderReportService;
use Modules\Order\Exceptions\CseCreateOrderException;

/**
 * Основной сервисы для работы с заказами КСЕ
 *
 * @author Oleg Pyatin
 */
class OrderService
{
    protected $cse_connector;

    protected $order_transfer_service;

    protected $order_check_service;

    protected $order_report_service;

    public function __construct(OrderTransferService $order_transfer_service, CseConnector $cse_connector,
                                OrderCheckService $order_check_service, OrderReportService $order_report_service)
    {
        $this->order_transfer_service = $order_transfer_service;

        $this->order_check_service = $order_check_service;

        $this->order_report_service = $order_report_service;

        $this->cse_connector = $cse_connector;
    }

    /**
     * Основная функция создания заказа в КСЕ
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа МС
     * @return string  ID нового заказа в системе КСЕ
     */
    public function createOrder(MwOrderData $mw_order_data)
    {
        $this->order_check_service->checkMwOrderData($mw_order_data);

        $cse_create_data = $this->order_transfer_service->getOrderData($mw_order_data);

        $cse_response = $this->cse_connector->processQuery("SaveDocuments", $cse_create_data->getPreparedData());

        if ($cse_order_id = $this->order_check_service->checkOrderCreateResult($cse_response)) {

            $this->order_report_service->doFinalActions($mw_order_data->id, $cse_order_id);
            return $cse_order_id;

        } else {
            throw new CseCreateOrderException("Error in creating order", 0, null, $mw_order_data->id);
        }
    }

    /**
     * Функция получения данных о заказе в системе КСЕ
     *
     * @param string $cse_order_id  ID заказа КСЕ
     * @return array  Данные о заказе
     */
    public function getOrderInfo(string $cse_order_id)
    {
        $get_order_params = [
            "GetDocuments"=>[
                'login'=>'test',
                'password'=>'2016',
                'data'=>[
                    'Key'=>'Number',
                    'List'=>[
                        'Key'=>$cse_order_id,
                        'Properties'=>[
                            'Key'=>'Number',
                            'Value'=>$cse_order_id,
                            'ValueType'=>'string'
                        ]
                    ]
                ],
                'parameters'=>[
                    'Key'=>'parameters',
                    'List'=>[
                        'Key'=>'DocumentType',
                        'Value'=>'Order',
                        'ValueType'=>'string'
                    ]
                ]
            ]
        ];

        $response_two = $this->cse_connector->processQuery("GetDocuments", $get_order_params);

        print_r($response_two);
    }
}
