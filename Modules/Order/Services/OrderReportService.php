<?php

namespace Modules\Order\Services;

use Modules\MyWarehouse\Components\MyWarehouseConnector;
use Modules\Order\Entities\OrderProcessValues;
use Modules\MyWarehouse\Entities\MwProcessValues;

/**
 * Сервис для выполнения действий после успешного создания заказа в КСЕ
 *
 * @author Oleg Pyatin
 */
class OrderReportService
{
    protected $mw_connector;

    public function __construct(MyWarehouseConnector $mw_connector)
    {
        $this->mw_connector = $mw_connector;
    }

    /**
     * Выполнение результирующих действия после создания заказа
     *
     * @param string $base_mw_order_number  ID заказа в системе Мой Склад
     * @param string $new_cse_order_id  ID нового созданного заказа в КСЕ
     * @return void  Просто выполняем действия и ничего не возвращаем
     */
    public function doFinalActions(string $base_mw_order_number, string $new_cse_order_id)
    {
        $this->saveReportToMw($base_mw_order_number, $new_cse_order_id);
    }

    /**
     * Функция отправки результирующих данных (маркировки, себестоимости и пр) в ERP
     *
     * @param string $base_mw_order_number  Номер заказа в системе МС
     * @param
     * @return void  Выполняем действие отправки и ничего не возвращаем
     */
    public function saveReportToMw(string $base_mw_order_number, string $new_cse_order_id)
    {
        $mw_additional_status_data = $this->mw_connector->sendSimpleQuery(MwProcessValues::MW_GET_ADDITIONAL_STATUS_URI);

        $this->mw_connector->updateDataQuery(OrderProcessValues::MW_ORDER_CHANGE_URL.$base_mw_order_number, [
            'attributes'=>[
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_TC_ORDER_NUMBER,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$new_cse_order_id
                ],
//                [
//                    'meta'=>[
//                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_CSE_WAYBILL,   // Нужно будет настроить на получение накладной
//                        'type'=>'attributemetadata',
//                        'mediaType'=>'application/json',
//                    ],
//                    'value'=>''
//                ],
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_ERROR_MESSAGE,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>''
                ],
                [
                    'meta'=>[
                        'href'=> MwProcessValues::MW_ORDER_CHANGE_ATTR_URL . MwProcessValues::MW_ATTR_ADDITIONAL_STATUS,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>[
                        'meta'=>$mw_additional_status_data["meta"],
                        'name'=>$mw_additional_status_data["name"],
                    ]
                ],
            ],
            'state'=>[
                'meta'=>[
                    'href'=>OrderProcessValues::MW_ORDER_CHANGE_STATE_URL . OrderProcessValues::MW_STATE_CSE_ORDER_CREATED,
                    'type'=>'state',
                    'mediaType'=>'application/json'
                ]
            ]
        ]);
    }
}
