<?php

namespace Modules\Order\Services;

use Modules\Order\Entities\OrderProcessValues;
use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\Order\Exceptions\CseRecipientCityEmptyException;
use Modules\Order\Exceptions\CseRecipientNotFullContactsException;

/**
 * Сервис для организации логики проверки данных
 *
 * @author Oleg Pyatin
 */
class OrderCheckService
{
    /**
     * Функция проверки корректности данных заказа МойСКлад (для обычного API Яндекса)
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     * @return void  Просто делаем проверку с выбросом исключения, если нужно
     */
    public function checkMwOrderData(MwOrderData $mw_order_data)
    {
        $this->checkRecipientContacts($mw_order_data);

        $this->checkDeliveryAddress($mw_order_data);
    }

    /**
     * Проверка корректного заполнения данных для пользователя (телефона)
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     */
    private function checkRecipientContacts(MwOrderData $mw_order_data)
    {
        if (empty($mw_order_data->agent["phone"]) && empty($mw_order_data->attributes[OrderProcessValues::MW_ATTR_RECIPIENT_PHONE])) {
            throw new CseRecipientNotFullContactsException("No info about phone", 0, null, $mw_order_data->id);
        }
    }

    /**
     * Проверка что заполнен адрес доставки
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     * @return void  Просто делаем проверку с выбросом исключения, если нужно
     */
    private function checkDeliveryAddress(MwOrderData $mw_order_data)
    {
        if (empty($mw_order_data->attributes[OrderProcessValues::MW_ATTR_RECIPIENT_CITY])) {
            throw new CseRecipientCityEmptyException("No info about recipient city", 0, null, $mw_order_data->id);
        }
    }

    /**
     * Проверка что заказ был создан
     *
     * @param object $order_create_result  Данные о результате попытки создания заказа
     * @return void  Просто делаем проверку с выбросом исключения, если нужно
     */
    public function checkOrderCreateResult(object $order_create_result)
    {
        try {
            $new_order_number = $order_create_result->return->List->Properties[2]->Value;
            return $new_order_number;
        } catch (\Throwable $exc) {
            return false;
        }
    }
}
