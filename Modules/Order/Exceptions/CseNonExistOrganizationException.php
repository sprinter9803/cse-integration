<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CseOrderCreatingException;

class CseNonExistOrganizationException extends CseOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Для отправки заказа используется организация, не учтенная в системе (Колорс или Китчентрэйд)';
    }
}
