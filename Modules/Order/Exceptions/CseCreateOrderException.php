<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CseOrderCreatingException;

class CseCreateOrderException extends CseOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Возникла ошибка при формировании заказа';
    }
}
