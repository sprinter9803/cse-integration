<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CseOrderCreatingException;

class CseRecipientNotFullContactsException extends CseOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Ошибка - Не указан телефон получателя';
    }
}
