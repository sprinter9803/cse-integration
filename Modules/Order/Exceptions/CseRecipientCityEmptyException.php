<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CseOrderCreatingException;

class CseRecipientCityEmptyException extends CseOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Ошибка - Не указан город доставки';
    }
}
