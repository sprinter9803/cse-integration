<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Order\Services\OrderService;
use Modules\MyWarehouse\Services\MwService;

class OrderController extends Controller
{
    protected $order_service;

    protected $mw_service;

    public function __construct(OrderService $order_service, MwService $mw_service)
    {
        $this->order_service = $order_service;
        $this->mw_service = $mw_service;
    }

    /**
     * Действие регистрации заказа в системе КСЕ
     * @param $mw_order_uuid  UUID заказа в системе Мой Склад
     * @return Renderable
     */
    public function createOrder(string $mw_order_uuid)
    {
        $mw_order_data = $this->mw_service->getMwOrderData($mw_order_uuid);
        return $this->order_service->createOrder($mw_order_data);
    }


    public function getOrderInfo(string $cse_order_id)
    {
        return $this->order_service->getOrderInfo($cse_order_id);
    }
}
