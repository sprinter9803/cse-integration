<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'order'], function () {
    Route::post('create/{mw_order_uuid}', 'OrderController@createOrder')->name('order.create');
    Route::get('info/{cse_order_id}', 'OrderController@getOrderInfo')->name('order.info');

    // Route::post('create/{mw_order_uuid}', 'OrderController@create')->middleware('supervisor.launch.record.start')->name('order.create');
});

