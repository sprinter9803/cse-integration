<?php

namespace Modules\MyWarehouse\Services;

use Modules\MyWarehouse\Components\OrderLoader;
use Modules\MyWarehouse\Components\MyWarehouseConnector;
use Modules\MyWarehouse\Entities\MwProcessValues;
use Modules\MyWarehouse\Entities\MwOrderData;

/**
 * Сервис для выполнения действий связанных с системой МойСклад
 *
 * @author Oleg Pyatin
 */
class MwService
{
    protected $mw_loader;

    public function __construct(OrderLoader $mw_loader, MyWarehouseConnector $mw_connector)
    {
        $this->mw_loader = $mw_loader;
        $this->mw_connector = $mw_connector;
    }

    public function getMwOrderData(string $mw_order_uuid)
    {
        $base_data = MwOrderData::loadFromArray($this->mw_connector->sendSimpleQuery(MwProcessValues::GET_ORDER_INFO_URL.
                $mw_order_uuid.MwProcessValues::EXTRA_ENTITY_INFO));

        return $this->prepareAttributesValues($base_data);
    }

    public function prepareAttributesValues(MwOrderData $base_data)
    {
        array_walk($base_data->attributes, function (&$value) {
            $value = [
                'id'=>$value["id"],
                'value'=>$value["value"]
            ];
        });

        $base_data->attributes = array_column($base_data->attributes, 'value', 'id');
        return $base_data;
    }

    /**
     * Функция получения тарифа для посылки - сначала пробуем взять из статичной таблицы, если тариф
     *     какой-то другой делаем запрос к МС
     *
     * @param string $tariff_name  Название тарифа
     * @param string $tariff_uri    URI для получения данных тарифа если не нашлось в справочнике
     * @return string  Код тарифа в системе СДЕК
     * @throws Exception  В случае если ничего не нашлось делаем исключение
     */
    public function getTariffCode(string $tariff_name, string $tariff_uri): string
    {
        // Сперва можно посмотреть по табличке - если не нашлось используем запрос
        if ($code = $this->getTariffCodeByName($tariff_name)) {
            return $code;
        }

        $tariff_code_from_mw = $this->getTariffByHref($tariff_uri)["code"] ?? false;

        if ($tariff_code_from_mw) {
            return $tariff_code_from_mw;
        } else {
            throw new Exception(MwProcessValues::ERROR_GET_TARIFF_DATA);
        }
    }

    public function getTariffByHref(string $tariff_uri)
    {
        return $this->mw_connector->sendSimpleQuery($tariff_uri);
    }

    public function prepareAssortmentAttrs(array $assortment_attributes)
    {
        array_walk($assortment_attributes, function (&$value) {
            $value = [
                'id'=>$value["id"],
                'value'=>$value["value"]
            ];
        });

        $prepared_assortment_attributes = array_column($assortment_attributes, 'value', 'id');
        return $prepared_assortment_attributes;
    }

    public function getCityInfoByHref(string $tariff_uri)
    {
        return $this->mw_connector->sendSimpleQuery($tariff_uri);
    }
}
